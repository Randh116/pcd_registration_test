import open3d as o3d
import numpy as np
import copy
import os
from PyInquirer import prompt

def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])


def preprocess_point_cloud(pcd, voxel_size):
    print(":: Downsample with a voxel size %.3f." % voxel_size)
    pcd_down = pcd.voxel_down_sample(voxel_size)

    radius_normal = voxel_size * 2
    print(":: Estimate normal with search radius %.3f." % radius_normal)
    pcd_down.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

    radius_feature = voxel_size * 5
    print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = o3d.registration.compute_fpfh_feature(
        pcd_down,
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
    return pcd_down, pcd_fpfh

def preprocess_without_downsample(pcd_estimated, voxel_size):
    radius_normal = voxel_size * 2
    print(":: Estimate normal with search radius %.3f." % radius_normal)
    pcd_estimated.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

    radius_feature = voxel_size * 5
    print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_estimated_fpfh = o3d.registration.compute_fpfh_feature(
        pcd_estimated,
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
    return pcd_estimated, pcd_estimated_fpfh

def prepare_dataset(source_path, target_path, voxel_size):
    print(":: Load two point clouds and disturb initial pose.")
    source = o3d.io.read_point_cloud(source_path)
    target = o3d.io.read_point_cloud(target_path)
    trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0],
                             [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
    source.transform(trans_init)
    draw_registration_result(source, target, np.identity(4))

    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
    return source, target, source_down, target_down, source_fpfh, target_fpfh


def prepare_dataset_with_scaling(source_path, target_path, voxel_size, scale_coeff):
    print(":: Load two point clouds and disturb initial pose.")
    source = o3d.io.read_point_cloud(source_path)
    target = o3d.io.read_point_cloud(target_path)
    scaled_target = o3d.geometry.PointCloud.scale(target, scale_coeff, center=True)
    trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0],
                             [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
    source.transform(trans_init)
    draw_registration_result(source, scaled_target, np.identity(4))

    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    scaled_target_down, scaled_target_fpfh = preprocess_point_cloud(scaled_target, voxel_size)
    return source, scaled_target, source_down, scaled_target_down, source_fpfh, scaled_target_fpfh

def execute_global_registration(source_down, target_down, source_fpfh,
                                target_fpfh, voxel_size):
    distance_threshold = voxel_size * 1.5
    print(":: RANSAC registration on downsampled point clouds.")
    print("   Since the downsampling voxel size is %.3f," % voxel_size)
    print("   we use a liberal distance threshold %.3f." % distance_threshold)
    result = o3d.registration.registration_ransac_based_on_feature_matching(
        source_down, target_down, source_fpfh, target_fpfh, distance_threshold,
        o3d.registration.TransformationEstimationPointToPoint(False), 4, [
            o3d.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
            o3d.registration.CorrespondenceCheckerBasedOnDistance(
                distance_threshold)
        ], o3d.registration.RANSACConvergenceCriteria(4000000, 500))
    return result


def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.4
    print(":: Point-to-plane ICP registration is applied on original point")
    print("   clouds to refine the alignment. This time we use a strict")
    print("   distance threshold %.3f." % distance_threshold)
    result = o3d.registration.registration_icp(
        source, target, distance_threshold, result_ransac.transformation,
        o3d.registration.TransformationEstimationPointToPlane())
    return result


def validate_normals(source, source_fpfh, target, target_fpfh, voxel_size):
    source_hasNormal = source.has_normals()
    target_hasNormal = target.has_normals()
    if (not source_hasNormal and not target_hasNormal):
        source_estimated, source_estimated_fpfh = preprocess_without_downsample(source, voxel_size)
        target_estimated, target_estimated_fpfh = preprocess_without_downsample(target, voxel_size)
        return source_estimated, source_estimated_fpfh, target_estimated, target_estimated_fpfh
    else:
        return source, source_fpfh, target, target_fpfh


def menu_inquire():
    targetQ = [
        {
            'type': 'list',
            'name': 'target',
            'message': 'Which target do you want to use?',
            'choices': [
                'Part of source pointcloud',
                'Model-sphere'
           ]
        }
    ]

    sizeQ = [
        {
            'type': 'list',
            'name': 'size',
            'message': 'Which size of sphere to you want to test?',
            'choices': [
                'small sphere',
                'small sphere on a hand',
                'medium sphere',
                'medium sphere on a hand',
                'big sphere',
                'big sphere on a hand',
                'extra: face model'
            ]
        }
    ]

    target = prompt(targetQ)
    size = prompt(sizeQ)
    #pprint(target)
    #pprint(size)

    if (target == {'target': 'Part of source pointcloud'}):
        return True, size
    else:
        return False, size


def deliver_data_basic(size):
    currentDirectory = os.getcwd()
    print(currentDirectory)

    if (size == {'size': 'small sphere'}):
        source_path = currentDirectory + "\\pointclouds\\kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\kleinkugel.ply"
        return source_path, target_path, 1
    elif (size == {'size': 'small sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\hand-kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\kleinkugel.ply"
        return source_path, target_path, 2
    elif (size == {'size': 'medium sphere'}):
        source_path = currentDirectory + "\\pointclouds\\tennisball-side-noise.ply"
        target_path = currentDirectory + "\\pointclouds\\tennisball-side.ply"
        return source_path, target_path, 3
    elif (size == {'size': 'medium sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\tennisball-bird-original.ply"
        target_path = currentDirectory + "\\pointclouds\\tennisball.ply"
        return source_path, target_path, 4
    elif (size == {'size': 'big sphere'}):
        source_path = currentDirectory + "\\pointclouds\\grosskugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\grosskugel.ply"
        return source_path, target_path, 5
    elif (size == {'size': 'big sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\hand-grosskugel.ply"
        target_path = currentDirectory + "\\pointclouds\\grosskugel.ply"
        return source_path, target_path, 6
    else:
        source_path = currentDirectory + "\\pointclouds\\02-face.ply"
        target_path = currentDirectory + "\\pointclouds\\02-cropped-face.ply"
        return source_path, target_path, 0


def deliver_data_model(size):
    currentDirectory = os.getcwd()
    print(currentDirectory)

    if (size == {'size': 'small sphere'}):
        source_path = currentDirectory + "\\pointclouds\\kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 7
    elif (size == {'size': 'small sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\hand-kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 8
    elif (size == {'size': 'medium sphere'}):
        source_path = currentDirectory + "\\pointclouds\\tennisball-side-noise.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 9
    elif (size == {'size': 'medium sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\tennisball-bird-original.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 10
    elif (size == {'size': 'big sphere'}):
        source_path = currentDirectory + "\\pointclouds\\grosskugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 11
    elif (size == {'size': 'big sphere on a hand'}):
        source_path = currentDirectory + "\\pointclouds\\hand-grosskugel.ply"
        target_path = currentDirectory + "\\pointclouds\\model-sphere.pcd"
        return source_path, target_path, 12
    else:
        source_path = currentDirectory + "\\pointclouds\\02-face.ply"
        target_path = currentDirectory + "\\pointclouds\\02-cropped-face.ply"
        return source_path, target_path, 0


def calibrate_coefficient(choice):
    if choice == 7 or choice == 8:
        scale_coeff = 0.2
        voxel_size = 0.03
    elif choice == 9 or choice == 10:
        scale_coeff = 0.4
        voxel_size = 0.02
    elif choice == 11 or choice == 12:
        scale_coeff = 0.6
        voxel_size = 0.015
    else:
        scale_coeff = 1
        voxel_size = 0.0125

    return scale_coeff, voxel_size

# not used in this version. this is an old user interface
def menu():

    currentDirectory = os.getcwd()
    print(currentDirectory)

    choice = input("""
                            what do you want to test?

                           1: face-registration 
                           2: small sphere
                           3: small sphere on a hand
                           4: medium sphere
                           5: medium sphere on a hand
                           6: big sphere
                           7: big sphere on a hand

                            please enter your choice: """)

    if choice == "1":
        source_path = currentDirectory + "\\pointclouds\\02-face.ply"
        target_path = currentDirectory + "\\pointclouds\\02-cropped-face.ply"
        return source_path, target_path
    elif choice == "2":
        source_path = currentDirectory + "\\pointclouds\\kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\kleinkugel.ply"
        return source_path, target_path
    elif choice == "3":
        source_path = currentDirectory + "\\pointclouds\\hand-kleinkugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\kleinkugel.ply"
        return source_path, target_path
    elif choice == "4":
        source_path = currentDirectory + "\\pointclouds\\tennisball-side-noise.ply"
        target_path = currentDirectory + "\\pointclouds\\tennisball-side.ply"
        return source_path, target_path
    elif choice == "5":
        source_path = currentDirectory + "\\pointclouds\\tennisball-bird-original.ply"
        target_path = currentDirectory + "\\pointclouds\\tennisball-bird.ply"
        return source_path, target_path
    elif choice == "6":
        source_path = currentDirectory + "\\pointclouds\\grosskugel-original.ply"
        target_path = currentDirectory + "\\pointclouds\\grosskugel.ply"
        return source_path, target_path
    elif choice == "7":
        source_path = currentDirectory + "\\pointclouds\\hand-grosskugel.ply"
        target_path = currentDirectory + "\\pointclouds\\grosskugel.ply"
        return source_path, target_path
    else:
        print("select a number from menu")
        print("please try again")
        menu()


if __name__ == "__main__":
    # voxel_size = float(input("choose the voxel size (downsampling factor) in range 0.05 - 0.0125: "))

    # specify, which target type do you want to test and call proper method
    mode, size = menu_inquire()
    if mode:
        source_path, target_path, choice = deliver_data_basic(size)
    else:
        source_path, target_path, choice = deliver_data_model(size)

    print(source_path)
    print(target_path)

    # calibrate the coefficient to properly calibrate the target sphere
    scale_coeff, voxel_size = calibrate_coefficient(choice)

    # call proper method depending on the choice
    if choice in range(0, 6):
        source, target, source_down, target_down, source_fpfh, target_fpfh = \
            prepare_dataset(source_path, target_path, voxel_size)
    else:
        source, target, source_down, target_down, source_fpfh, target_fpfh = \
            prepare_dataset_with_scaling(source_path, target_path, voxel_size, scale_coeff)

    result_ransac = execute_global_registration(source_down, target_down,
                                                source_fpfh, target_fpfh,
                                                voxel_size)
    print(result_ransac)

    draw_registration_result(source_down, target_down,
                             result_ransac.transformation)

    source_estimated, source_estimated_fpfh = preprocess_without_downsample(source, voxel_size)
    target_estimated, target_estimated_fpfh = preprocess_without_downsample(target, voxel_size)

    result_icp = refine_registration(source_estimated, target_estimated,
                                     source_estimated_fpfh, target_estimated_fpfh,
                                     voxel_size)
    print(result_icp)

    draw_registration_result(source_estimated, target_estimated,
                             result_icp.transformation)

    # print("source coordinates: {coord}".format(coord=np.asarray(source.points)))
    # print("target coordinates: {coord}".format(coord=np.asarray(target.points)))

    # ransac_corr_set = np.asarray(result_ransac.transformation)
    # print(ransac_corr_set)
    # icp_corr_set = np.asarray(result_icp.transformation)
    # print(icp_corr_set)