04.03.2020

1. download PyCharm Community Edition - the Pro Edition is in a trial licence
2. download and install Python 3.6.5, locate it in C:\Python\
	-> you can test whether everything is correctly installed by typing for example "python --version" - should give response
	-> evtl. upgrade pip - its good to do it
		-> in cmd type: python -m pip install --upgrade pip
3. go to cmd and type:
	-> pip install numpy - it'll install the newest version of the package (v 1.18.1)
	-> pip install open3d (v 0.9.0.0)
4. be sure that you have already installed Visual Studio Redistributable C++ 2015, 2017, 2019 pack - it is needed for the open3d c++ backend to work properly
5. just click on start_registration.bat

everything should be working properly.



-----------------------------------------------------------------------------------
if you want to modify and try to open the scripts in PyCharm environment:

1. configure PyCharm - open existing project (global_registration_fulltest.py)
2. in PyCharm:
	File -> Settings -> Project: Advanced -> Project Interpreter -> Gear Icon -> Add...
	In the Add Python Interpreter Window:
		Virtualenv Environment -> New environment 
		-> set location to the file, in which you have global_registration_fulltest.py
		-> set base interpreter to the python.exe file in v3.6.5 (should be C:\Python\python.exe\)
		-> check the "Inherit global site-packages" checkbox - it'll import to your virtual environment the numpy and open3d packages you previously installed by pip
		-> OK -> Apply Changes
3. in the upper right corner you can see: "Edit Configuration..." - choose there the config your just set
4. try to run the script - it should work - but it can depend on the system version or the package config 